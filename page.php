<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Urbana_Theme
 */

get_header(); ?>

	<?php if ( is_front_page() ) : ?>

		<?php 
			get_template_part( 'template-parts/home_slider' ); 
			get_template_part( 'template-parts/reservation_form' );
		?>

	<?php endif; ?>

	<div id="primary" class="container content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php 
					if ( is_front_page() ) {
						get_template_part ( 'template-parts/content', 'home' );
					} else {
						get_template_part( 'template-parts/content', 'page' ); 
					}
				?>

				<?php
					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
				?>

			<?php endwhile; // End of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php 
	if ( ! is_front_page() ) {
		get_sidebar(); 
	}
?>

<?php get_footer(); ?>
