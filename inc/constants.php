<?php 

/**
 * Urbana Theme Constants.
 *
 * @package Urbana_Theme
 */

/**
 * Define Image directory constact
 */
define( 'URBANA_IMG_DIR', get_stylesheet_directory_uri() . '/images' );