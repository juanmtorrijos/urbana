<?php 
/**
 * Template part for navigation.
 *
 * This is the template part that displays the primary-navigation of the theme.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package  Urbana_Theme
 */

?>

<header id="masthead" class="urbana-header" role="banner">
	<div class="urbana-navigation container">
		<div class="row">
			<div class="urbana-branding four columns">
			
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
					<img src="<?php echo URBANA_IMG_DIR; ?>/urbana-logo.svg" alt="Urbana Logo">
				</a>

				<?php

				$description = get_bloginfo( 'description', 'display' );
				if ( $description || is_customize_preview() ) : ?>
					<p class="urbana-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
				<?php endif; ?>
			</div><!-- .urbana-branding -->

			<nav id="urbana-navigation" class="urbana-main-navigation eight columns" role="navigation">
				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
			</nav><!-- #urbana-navigation -->
		</div>
	</div>
</header><!-- #masthead -->