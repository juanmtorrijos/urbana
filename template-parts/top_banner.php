<?php 
/**
 * Template part for top banner.
 *
 * This is the template part that displays the top most banner in our theme
 * In this banner we display the location, company phone number for reservation and a language switcher.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package  Urbana_Theme
 *
 * @todo  Make dynamic with options or widgets
 */

?>
<div class="urbana-top-banner">
	<div class="container">
		<div class="row">
			<div class="six columns urbana-location">
				<p>SOHO 555, Broadway, New York, NY</p>
			</div>
			<div class="six columns urbana-language">
				<ul>
					<li class="urbana-num">
						INFO & RESERVATION: +01 2345 67890
					</li>
					<li href="#0" class="urbana-language-switcher">
						US English
						<ul class="languages">
							<li class="french"><a href="#0">French</a></li>
							<li class="italian"><a href="#0">Italian</a></li>
							<li class="spanish"><a href="#0">Spanish</a></li>
							<li class="german"><a href="#0">German</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>