<?php 
/**
 * Template part for home slider.
 *
 * This is the template part that displays the home slider or banner.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package  Urbana_Theme
 *
 * @todo  Change static content to options or custom post type.
 */

?>

<div class="urbana-slider">
	<div class="urbana-ken-burns" id="urbana-ken-burns">
		<img src="<?php echo URBANA_IMG_DIR; ?>/slider/slider-bg-1.jpg" alt="">
		<img src="<?php echo URBANA_IMG_DIR; ?>/slider/slider-bg-2.jpg" alt="">
		<img src="<?php echo URBANA_IMG_DIR; ?>/slider/slider-bg-3.jpg" alt="">
	</div>
	<div class="urbana-slider-text">
		<h1><?php echo esc_html__('The Place to Stay in the Big City', 'urbana'); ?></h1>
		<p><?php echo esc_html__('Starting at $149/night', 'urbana'); ?></p>
		<a class="urbana-slider-button" id="urbana-slider-button" href="#0"><?php echo esc_html__('Make Reservation', 'urbana'); ?></a>
	</div>
</div>