<?php
/**
 * Template part for displaying page content in page.php.
 * It will be displayed for the home page.
 * Remember change the front page to a static page called home
 *
 * @link https://codex.wordpress.org/Creating_a_Static_Front_Page
 * 
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Urbana_Theme
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="row">
		<header class="home-header three columns">
			<h1><?php echo get_bloginfo( 'name', 'display' ); ?></h1>
		</header><!-- .entry-header -->

		<div class="home-content nine columns">
			<?php the_content(); ?>
			<?php
				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'urbana' ),
					'after'  => '</div>',
				) );
			?>
		</div><!-- .entry-content -->
	</div>

	<footer class="entry-footer">
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					esc_html__( 'Edit %s', 'urbana' ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->

