<?php 
/**
 * Template part for home reservation form.
 *
 * This template part will display the reservation form under the slider at the home page.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package  Urbana_Theme
 *
 * @todo  Make active with post request. 
 */
?>

<div class="urbana-reservation-form" id="urbana-reservation-form">
	<div class="container">
		<form action="" method="post">
			<div class="row">
			
				<input type="text" class="urbana-datepicker" id="urbana-check-in" name="urbanaCheckIn" placeholder="Check In">
			
			
				<input type="text" class="urbana-datepicker" id="urbana-check-out" name="urbanaCheckOut" placeholder="Check Out">
			
			
				<select class="urbana-select" name="urbanaAdults" placeholder="2 Adults">
					<option value="0 Adults">0 Adults</option>
					<option value="1 Adult">1 Adult</option>
					<option value="2 Adults" selected>2 Adults</option>
					<option value="3 Adults">3 Adults</option>
					<option value="4 Adults">4 Adults</option>
				</select>
			
			
				<select class="urbana-select" name="urbanaChildren" placeholder="2 Children">
					<option value="0 Children">0 Children</option>
					<option value="1 Child" selected>1 Child</option>
					<option value="2 Children">2 Children</option>
					<option value="3 Children">3 Children</option>
					<option value="4 Children">4 Children</option>
				</select>
			
			
				<input type="submit" class="urbana-reservation-button" value="Check Availability">
				
			</div>
		</form>
	</div>
</div>