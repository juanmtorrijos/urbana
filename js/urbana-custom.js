;(function( $ ) {
$(function() {

var urbanaSliderButton = $('#urbana-slider-button'),
	urbanaReservationForm = $('#urbana-reservation-form'),
	urbanaCheckIn = $('#urbana-check-in'),
	urbanaCheckOut = $('#urbana-check-out'),
	urbanaKenBurns = $('#urbana-ken-burns');

function goToReservationForm(e){
	e.preventDefault();
	urbanaReservationForm.velocity('scroll');
	urbanaCheckIn.focus();
}

urbanaSliderButton.on('click', goToReservationForm);
	
	urbanaCheckIn.datepicker({
		minDate: 0,
		maxDate: "+6M",
		dateFormat: "d MM, yy",
	});

	urbanaCheckOut.datepicker({
		minDate: +1,
		maxDate: "+6M",
		dateFormat: "d MM, yy",
	});

	urbanaKenBurns.kenburnsy();


});
})(jQuery);