<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Urbana_Theme
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="urbana-footer" role="contentinfo">
		<div class="urbana-info">
			
			<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'urbana' ) ); ?>"><?php printf( esc_html__( 'Proudly powered by %s', 'urbana' ), 'WordPress' ); ?></a>
			<span class="sep"> | </span>
			<?php printf( esc_html__( 'Theme: %1$s by %2$s.', 'urbana' ), 'Urbana', '<a href="http://moitorrijos.com" rel="designer">Moi Torrijos</a>' ); ?>

			<span class="flaticon-attribution">Icons made by <a href="http://www.freepik.com" title="Freepik">Freepik</a> from <a href="http://www.flaticon.com" title="Flaticon">www.flaticon.com</a> is licensed under <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0">CC BY 3.0</a></span>

		</div><!-- .urbana-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
