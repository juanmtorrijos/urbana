var gulp 		= require('gulp');
var browserSync = require('browser-sync');
var reload 		= browserSync.reload;
var sass 		= require('gulp-sass');

gulp.task('browser-sync', ['sass'], function(){

	var files = [
		'styles.css',
		'*.php'
	];
	
	browserSync.init(files, {
		ui: {
			port: 8000,
		},

		proxy: "http://urbana:8888/"
	});
});

gulp.task('sass', function(){
	return gulp.src('sass/*.scss')
		.pipe(sass())
		.pipe(gulp.dest('./'))
		.pipe(reload({stream:true}));
});

gulp.task('default', ['sass', 'browser-sync'], function(){
	gulp.watch('sass/**/*.scss', ['sass']);
	gulp.watch(['*.php', 'template-parts/*.php']).on('change', reload);
});